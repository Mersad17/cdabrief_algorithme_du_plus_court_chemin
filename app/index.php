<link rel="stylesheet" href="CSS/main.css" type="text/css">
<?php
// On the line below, create your own associative array:
// $myArray = array (  'Early (Payment received by 1/4/15)' => array('MD/DO', '$23','RNA/PA', '$37','RESIDENT/RN/OTHERS', '$49'),
//                    'Early (Payment received by 1/4/15) ' => array('MD/DO', '$23','RNA/PA', '$37','RESIDENT/RN/OTHERS', '$49'));
$column =10;
$row =10;

$arrayColumn = array();
$arrayRow = array();

// Put the map in a array $arrayColumn
for ($x=0; $x <$column ; $x++) { 
    $arrayColumn[] = $x;
    for ($y=0; $y <$row ; $y++) { 
        $arrayRow[] = $y;
    }
    $arrayColumn[$x]=$arrayRow;
    $arrayRow = [];
}

// Table of the map
// Blocked cordinations
$blocked = array();
array_push($blocked,12, 24,25,32);
print '<table width="800" cellpadding="1" cellspacing="1" border="1">';
foreach($arrayColumn as $key => $val)
{
   
    print "<tr>"; 
    echo "this is the key {$key} -> \n";
    foreach ($val as $valueOfColumn) {
        $id=$key."".$valueOfColumn;
        print($id);
        if (in_array($id, array_values($blocked))) {
            print $id.",".$key.",".$valueOfColumn;
            print "<td class=blockedRoad id=".$id.">".$key.",".$valueOfColumn."</td>";
        }else{
            print "<td id=".$key."".$valueOfColumn.">".$key.",".$valueOfColumn."</td>";
        }
    }
    print "</tr>";
}

print " </table>";
